package ru.remezov;

import java.util.*;

public class App {
    public static void main(String[] args) {
        start();
    }

    private static void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a text.");
        String text = scanner.nextLine();
        text = text.replaceAll(" ", "");
        text = text.toLowerCase();
        toMap(text);
    }

    private static void toMap(String text) {
        Map<Character, Integer> letter = new HashMap<>();
        for (int i = 0; i < text.length(); i++) {
            if (!letter.containsKey(text.charAt(i))) {
                int count = 0;
                for (int j = 0; j < text.length(); j++) {
                    if (text.charAt(i) == text.charAt(j)) {
                        count++;
                    }
                }
                letter.put(text.charAt(i), count);
            }
        }
        find(letter);
    }

    private static void find (Map<Character, Integer> letter) {
        //На случай если максимально встречающихся символов больше одного, вводим список для записи их ключей
        List<Character> keys = new ArrayList<>();
        char key = 0;
        for(Map.Entry<Character, Integer> entry1: letter.entrySet()) {
            key = entry1.getKey();
            for(Map.Entry<Character, Integer> entry2: letter.entrySet()) {
                if (entry1.getValue() < entry2.getValue()) {
                    key = entry2.getKey();
                }
            }
            if (entry1.getKey().equals(key)) {
                keys.add(key);
            }
        }
            System.out.println("Character: " + keys + " has occurred maximum times in String: " +
                    letter.get(keys.get(0)));
    }
}
